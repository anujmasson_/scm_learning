FROM python:3.7
WORKDIR /code
COPY . /code
RUN rm -rf venv
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "80"]
