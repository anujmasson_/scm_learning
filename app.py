from fastapi import FastAPI
import uvicorn
import traceback
from application.scripts.core.services.deviation_analysis_services import router

app = FastAPI()
app.include_router(router)

@app.get("/")
def read_root():
    return "welcome to the application"


# uvicorn --port=5000 --reload application:application
if __name__ == '__main__':
    try:
        uvicorn.run(app, host='0.0.0.0', port=5000)
    except:

        traceback.print_exc()