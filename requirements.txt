psycopg2 == 2.9.3
fastapi == 0.74.1
uvicorn == 0.17.5
pydantic == 1.9.0