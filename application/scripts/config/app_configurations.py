import configparser

# app_config = configparser.ConfigParser()
# app_config.read('application/conf/application.conf')

db_conf = configparser.ConfigParser()
db_conf.read('application/conf/db.conf')

api_base_service_url = "/api/nc/v1"

database = db_conf.get('Database', 'db_name')
password = db_conf.get('Database', 'password')
port = db_conf.get('Database', 'port')
host = db_conf.get('Database', 'host')
user = db_conf.get('Database', 'user')

# log_level = app_config.get('LOG', 'log_level')
# log_basepath = app_config.get('LOG', 'base_path')
# log_filename = log_basepath + app_config.get('LOG', 'file_name')
# log_handlers = app_config.get('LOG', 'handlers')
# logger_name = app_config.get('LOG', 'logger_name')