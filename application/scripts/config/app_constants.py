from application.scripts.config import app_configurations
from  datetime import  datetime

class DatabaseTables(object):
    cluster_output_long = "nc_deviation.cluster_output_long"
    processed_long = "nc_deviation.nc_deviation_processed_long"
    cluster_output_long_short = "nc.deviation.cluster_output_long_short"

class DeviationAnalysis(object):
    api_list_dropdown = app_configurations.api_base_service_url + "/list_dropdown"
    api_list_clusters = app_configurations.api_base_service_url + "/list_clusters"
    api_cluster_freq_terms = app_configurations.api_base_service_url + "/list_cluster_freq_terms"
    api_cluster_functional_area = app_configurations.api_base_service_url + "/list_cluster_functional_area"
    api_cluster_row_data = app_configurations.api_base_service_url + "/list_cluster_row_data"
    api_download_row_data = app_configurations.api_base_service_url + "/download_row_data"
    KEY_RESULT = "result"

def result_template(data):
    return {
        "status": "Success", "timestamp": datetime.now(), "response" :
    data
    }

class DDL:
    create_cluster_output_table_ddl = f"""CREATE TABLE IF NOT EXISTS "{DatabaseTables.cluster_output_long_short}"(                  
                     "sr_no" varchar(20),
                     "year" int,
                     "resolution_status" varchar(25),
                     "function" varchar(25),
                     "plant_code_observed" varchar(25),
                     "severity" varchar(25),
                     "cluster_name" varchar(25),
                     "key_phrases" varchar);"""

    insert_cluster_output_table_ddl = f""" INSERT INTO "{DatabaseTables.cluster_output_long_short}" (sr_no, year, resolution_status, function, plant_code_observed,
                                       severity, cluster_name, key_phrases) SELECT sr_no, DATEPART(year, observation_date), resolution_status, function, plant_code_observed, severity, cluster_name,
                                        key_phrases FROM "{DatabaseTables.cluster_output}"; """