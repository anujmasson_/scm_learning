from fastapi import HTTPException
from application.scripts.config import app_constants

class AdditionHandler(object):
    def __init__(self):
        """

        """
    
    def add_numbers(self, input):
        """

        :param input:
        :return:
        """
        try:
            num1 = input.num1
            num2 = input.num2
            result = {app_constants.Addition.KEY_RESULT: num1 + num2}
            return result

        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    def mul_numbers(self, input):
        """

        :param input:
        :return:
        """
        try:
            num1 = input.num1
            num2 = input.num2
            result = {app_constants.Addition.KEY_RESULT: num1 * num2}
            return result

        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

