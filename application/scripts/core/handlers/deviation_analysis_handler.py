from fastapi import HTTPException
from application.scripts.config import app_constants
# from application.scripts.logging.app_loggers import logger
from application.scripts.utilities.redshift_utility import RedShiftUtility
import traceback

database_object = RedShiftUtility()

class DeviationAnalysisHandler(object):
    def __init__(self):
        """

        """

    def list_dropdown(self):
        try:
            """
            Function to fetch all dropdown values by default
            :return dropdown
            """

            year = f""" select distinct year from {app_constants.DatabaseTables.cluster_output_long} """
            severity = f""" select distinct severity from {app_constants.DatabaseTables.cluster_output_long} """
            res_status = f""" select distinct resolution_status from {app_constants.DatabaseTables.cluster_output_long} """
            plant_code = f""" select distinct plant_code_observed from {app_constants.DatabaseTables.cluster_output_long} """
            year_query = database_object.execute_select_query(year)
            severity_query = database_object.execute_select_query(severity)
            res_status_query = database_object.execute_select_query(res_status)
            plant_code_query = database_object.execute_select_query(plant_code)
            year_data = [item for t in year_query for item in t]
            severity_data = [item for t in severity_query for item in t]
            res_status_data = [item for t in res_status_query for item in t]
            plant_code_data = [item for t in plant_code_query for item in t]

            result = {"year": year_data,
                      "severity": severity_data,
                      "resolution_data": res_status_data,
                      "plant_code_observed":plant_code_data
                  }

            return app_constants.result_template(result)

        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching dropdown options" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def get_clusters(self, year, severity, resolution_status, plant_code):
        try:
            """
            Function to fetch all clusters based on query params
            :return no of clusters 
            """
            if len(year) == 1:
                year_condition = f""" year in ({year[0]})"""
            else:
                year_condition = f""" year in {tuple(year)}"""

            if len(resolution_status) == 1:
                res_status_condition = f""" resolution_status in ({resolution_status[0]}) """
            else:
                res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if len(plant_code) == 1:
                plant_code_condition = f""" plant_code_observed in ({plant_code[0]}) """
            else:
                plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} """


            cluster_info = []
            query = f""" select distinct cluster_name, total_deviations, x, y, top_10_terms, keyphrases from {app_constants.DatabaseTables.cluster_output_long}
                    where {year_condition} and severity = '{severity}' and {res_status_condition} and {plant_code_condition}  """
            data = database_object.execute_select_query(query)
            for each_cluster in data:
                number_of_clusters = data.index(each_cluster)+1
                cluster_info.append({
                    "cluster_name":each_cluster[0],
                    "total_deviations":each_cluster[2],
                    "x":each_cluster[3],
                    "y":each_cluster[4],
                    "top_10_terms":each_cluster[5]

                })
            result = {
                "number_of_clusters": number_of_clusters,
                "cluster_info": cluster_info
            }
            return app_constants.result_template(result)
        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occuerd while fetching no of clusters" + str(e))
            raise HTTPException(status_code=500, detail=str(e))

    def get_cluster_graph_data(self, year, severity, resolution_status, cluster_name, plant_code):
        try:
            """
            Function to fetch all frequent terms
            :return no of clusters with frequent terms
            """
            top_10_terms_query = f""" select keyphrases from {app_constants.DatabaseTables.cluster_info}  """
            top_10_freq_query = f""" select top_10_freq from {app_constants.DatabaseTables.cluster_info}  """
            top_10_terms = database_object.execute_select_query(top_10_terms_query)
            top_10_freq = database_object.execute_select_query(top_10_freq_query)

            from collections import Iterable
            import collections
            new_list = []
            new_list1 = []

            for x in top_10_terms:
                x[0]
                list1 = x[0].strip('"[],')
                w = list1.replace("'", "")
                z = list(w.split(","))
                new_list.append(z)
            for y in top_10_freq:
                list2 = y[0].strip('"[],')
                w1 = list2.replace("'", "")
                z1 = list(w1.split(","))
                new_list1.append(z1)
            data_result = []
            for index, val in enumerate(new_list):
                new_dict = dict(zip(new_list[index], new_list1[index]))
                data_result.append(new_dict)
            l = {}
            for item in data_result:
                for key in item.keys():
                    if key not in l:
                        b = {key: item[key]}
                        l.update(b)
                    else:
                        l[key] = str(int(l[key]) + int(item[key]))

            sorted_freq_terms = sorted(l.items(), key = lambda x:x[1])
            # print(sorted_freq_terms)

            year_condition = ""
            res_status_condition =""
            plant_code_condition = ""
            if year!=None:
                year = ','.join(map(str, year))
                year_condition = year_condition + f"""and year in ({year})"""

            if resolution_status!=None:
                resolution_status = ','.join(map(str,resolution_status))
                res_status_condition = res_status_condition +  f""" and resolution_status in ({resolution_status}) """
            # else:
            #     res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if plant_code!= None:
                plant_code = ','.join(map(str, plant_code))
                plant_code_condition = plant_code_condition  + f""" and plant_code_observed in ({plant_code}) """
            # else:
            #     plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} """

            query = f""" select info.top_10_terms, info.top_10_freq from {app_constants.DatabaseTables.cluster_info} info, {app_constants.DatabaseTables.cluster_output} out where info.cluster_name = '{cluster_name}' and out.severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition}"""
            graph_data_clustere_basis = database_object.execute_select_query(query)
            print(query)
            print("------------")

            result = {
                "frequenct_terms_all_cluster": sorted_freq_terms,
                "frequent_terms_particular_cluster":graph_data_clustere_basis
                # "functional_area_all_cluster": result_dict

            }
            return app_constants.result_template(result)

        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching cluster freq terms" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def get_cluster_functional_area(self, year, severity, resolution_status, cluster_name, plant_code):
        try:
            """
            Function to fetch all functional groups by default and based on cluster_name
            :return no of clusters with functional groups
            """
            functional_area_query = f""" select function_area, count(function_area) from {app_constants.DatabaseTables.processed_long} group by function_area """
            all_functional_area_query = database_object.execute_select_query(functional_area_query)
            # print(all_functional_area_query)
            functional_area_data = [item for t in all_functional_area_query for item in t]
            result_dict = {functional_area_data[i]: functional_area_data[i+1] for i in range(0, len(functional_area_data),2) }
            print(result_dict)

            year_condition = ""
            res_status_condition = ""
            plant_code_condition = ""
            if year != None:
                year = ','.join(map(str, year))
                year_condition = year_condition + f"""and DATEPART(year, observation_date) in ({year})"""
                print(year_condition)
            if resolution_status != None:
                resolution_status = ','.join(map(str, resolution_status))
                res_status_condition = res_status_condition + f""" and resolution_status in ('{resolution_status}') """
            # else:
            #     res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if plant_code != None:
                plant_code = ','.join(map(str, plant_code))
                print(plant_code)
                plant_code_condition = plant_code_condition  + f""" and plant_code_observed in ({plant_code}) """
            # else:
            #     plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} """

            cluster_functional_area = f""" select function_area, count(function_area) from {app_constants.DatabaseTables.cluster_output}  where cluster_name = '{cluster_name}' and severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition} group by function_observed  """
            graph_data_clustere_basis = database_object.execute_select_query(cluster_functional_area)
            print(graph_data_clustere_basis)
            print("+++++++++++++++++++++++++++++++++++")
            functional_area_data = [item for t in graph_data_clustere_basis for item in t]
            functional_area_specific_result = {functional_area_data[i]: functional_area_data[i + 1] for i in
                           range(0, len(functional_area_data), 2)}
            result = {

                "functional_area_all_cluster": result_dict,
                "functional_area_specific_cluster": functional_area_specific_result

            }
            return app_constants.result_template(result)


        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching cluster freq terms" + str(e))
            raise HTTPException(status_code=500, detail=str(e))

    def get_cluster_row_data(self,size, page_number, year, severity, resolution_status, cluster_name, plant_code):
        try:
            """
            Function to fetch entire cluster data based on cluster_name, year, severity, res_status, plant_code
            :return cluster details
            """
            year_condition = ""
            res_status_condition = ""
            plant_code_condition = ""
            if year != None:
                year = ','.join(map(str, year))
                year_condition = year_condition + f"""and year in ({year})"""

            if resolution_status != None:
                resolution_status = ','.join(map(str, resolution_status))
                res_status_condition = res_status_condition + f""" and resolution_status in ('{resolution_status}') """
            # else:
            #     res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if plant_code != None:
                plant_code = ','.join(map(str, plant_code))
                print(plant_code)
                plant_code_condition = plant_code_condition + f""" and plant_code_observed in ('{plant_code}') """
            # else:
            #     plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} """

            query = f""" select sr_no, short_explanation, long_explanation, observation_date, target_resolution_date, resolution_date,
                         quality_faillure_type, plant_code_observed, manufacturing_line_code_observed, function_observed, root_cause
                         from {app_constants.DatabaseTables.processed_long} where 
                         cluster_name = '{cluster_name}' and severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition}
                         ORDER BY sr_no limit {size} OFFSET {page_number * size}"""
            cluster_row_data = database_object.execute_select_query(query)
            cluster_data = []
            # if page_number is not None and size is not None:
            #     limit_condition = f" limit {size} offset {page_number * size - size} "
            # else:
            #     limit_condition = " "
            for each_cluster in cluster_row_data:
                number_of_clusters = cluster_row_data.index(each_cluster) + 1
                cluster_data.append({
                    "sr_no": each_cluster[0],
                    "short_explanation": each_cluster[1],
                    "long_explanation": each_cluster[2],
                    "observation_date": each_cluster[3],
                    "target_resolution_date": each_cluster[4],
                    "resolution_date": each_cluster[5],
                    "quality_faillure_type": each_cluster[6],
                    "plant_code_observed": each_cluster[7],
                    "manufacturing_line_code_observed": each_cluster[8],
                    "function_observed": each_cluster[9],
                    "root_cause": each_cluster[10]

                })
            result = {
                "number_of_clusters": number_of_clusters,
                "cluster_data":cluster_data
            }
            return app_constants.result_template(result)

        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching cluster row data" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def download_row_data(self):
        try:
            """
            Function to download entire cluster data in a file
            :return cluster details
            """
            pass
        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while downloading data" + str(e))
            raise HTTPException(status_code=500, detail=str(e))
