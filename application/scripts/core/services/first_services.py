from fastapi import APIRouter, HTTPException
from application.scripts.config import app_constants
from application.scripts.core.handlers.first_handler import AdditionHandler
from application.scripts.core.models import first_model

router = APIRouter()

@router.post(app_constants.Addition.api_add_numbers, response_model=first_model.AdditionOutput)
def add_number(input: first_model.AdditionInput):
    """

    :param input:
    :return:
    """

    try:
        obj = AdditionHandler()
        result = obj.add_numbers(input)
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post(app_constants.Addition.api_mul_number, response_model=first_model.MultiplicationOutput)
def mul_number(input: first_model.MultiplicationInput):
    """

    :param input:
    :return:
    """
    try:
        obj = AdditionHandler()
        result = obj.mul_numbers(input)
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
