from fastapi import APIRouter, HTTPException, Query
from application.scripts.config import app_constants
# from application.scripts.logging.app_loggers import logger
from application.scripts.core.handlers.deviation_analysis_handler import DeviationAnalysisHandler
from typing import Optional, List
router = APIRouter()

@router.get(app_constants.DeviationAnalysis.api_list_dropdown)
def list_dropdown():
    try:
        obj = DeviationAnalysisHandler()
        result = obj.list_dropdown()
        return result

    except Exception as e:
        # logger.exception('Exception in List Dropdown Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_list_clusters)
def get_clusters(severity: str, res_status: List[str] = Query(...), year: List[str] = Query(...), plant_code: List[str] = Query(...)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_clusters(year, severity, res_status, plant_code)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_cluster_freq_terms)
def get_cluster_graph( severity: Optional[str] = None , cluster_name:Optional[str] = None, res_status: List[str] = Query(None), plant_code: List[str] = Query(None), year: List[str] = Query(None)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_cluster_graph_data(year, severity, res_status, cluster_name, plant_code)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Frequent Terms Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_cluster_functional_area)
def get_functional_area( severity: Optional[str] = None , cluster_name:Optional[str] = None, res_status: List[str] = Query(None), plant_code: List[str] = Query(None), year: List[str] = Query(None)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_cluster_functional_area(year, severity, res_status, cluster_name, plant_code)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Functional Area Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_cluster_row_data)
def get_cluster_row_data(size:int, page_number: int, severity: Optional[str] = None , cluster_name:Optional[str] = None, res_status: List[str] = Query(None), plant_code: List[str] = Query(None), year: List[str] = Query(None)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_cluster_row_data(size, page_number, year, severity, res_status, cluster_name, plant_code)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Functional Area Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_download_row_data)
def get_cluster_row_data():
    try:
        obj = DeviationAnalysisHandler()
        result = obj.download_row_data()
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Functional Area Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))