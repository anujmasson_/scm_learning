from pydantic import BaseModel

class AdditionInput(BaseModel):
    num1: int
    num2: int

class MultiplicationInput(BaseModel):

    num1: int
    num2: int

class AdditionOutput(BaseModel):
    result: int

class MultiplicationOutput(BaseModel):
    result: int

class PersonInput(BaseModel):
    personid: str
    lastname: str
    firstname: str
    address: str
    city: str

class PersonOutput(BaseModel):
    result: dict
